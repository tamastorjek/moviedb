# Backend
- data sanitization and validation can be added
- dependency injection can be added
- the folder structure _might_ be changed to a more component based approach
- minor: adding `imports` in _package.json_ breaks autocomplete in the editor, it needs further investigation

# Frontend
- React has changed quite a lot since the last time I used it :)
- I tried to use slices instead of creating all actions/reducers separately, it seemed to be a better fit, maybe a large scale project/feature would need a different approach
- not all states are managed by the store
- maybe I should have included some class based components...
- _containers_ are the components which interact with the store
- I'm not sure if I have organized the code the best way

# Overall
- I didn't have time to write any test :(
- maybe I should have dockerized the whole project
- as far as I understand, GitHub Pages can only serve static sites and I also didn't have time to put it up. Maybe later I will check it out.
- even if you are not satisfied with the outcome, I would really appreciate some honest feedback, so I can learn
